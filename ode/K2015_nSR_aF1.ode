#**************************************************************************
# Koivumaki human Atrial Myocyte model - 2015 
# cAF-remodelled variant
#
# This code was adapted to gotran-form for M.Sc. thesis work by 
# Jonas van den Brink based on the original authors MATLAB implementation
# 
# Several simplifcations have been made
# - We only include square-shaped stimulus
# - The IKACh current has been ignored
#**************************************************************************

#**************************************************************************
# Human atrial myocyte model
#
# Citation: N/A
#
# -------------------------------------------------------------------------
# - Modifications to 2014 version:
#   * reformulated sodium current, INa (including late current)
#   * small conductance calcium activated potassium current, IKCa
#   * included, IKACh as in Voigt et al. (2013)
#   * updated IKr and IKs formulations as in Grandi et al. (2011)
#   * updated NKA formulation as in Grandi et al. (2011)
#   * increased Ito conductance and reformulated ICaL to get a more
#     spike-and-dome like AP shape
#   * dependence of ECa_app on Ca_ext as in Campbell et al. (1988)
#   * temperature adjusted to 37C (from 33C)
#   * cAF model variant extended to include:
#     > reduced INa
#     > reduced IKCNa
#     > increased IKs
# 
# -------------------------------------------------------------------------
# Model pedigree:
#
# Koivumaeki JT, Seemann G, Maleckar MM, Tavi P (2014)
# In Silico Screening of the Key Cellular Remodeling Targets in Chronic
# Atrial Fibrillation.
# PLoS Comput Biol 10(5): e1003620.
# http://dx.doi.org/10.1371/journal.pcbi.1003620
#
# - Modifications to 2011 version:
#   * reformulated L-type calcium current
#   * SERCA pump with explicit PLB and SLN effects
#   * corrected Ito and IKur conductances
#
# Koivumaeki JT, Korhonen T, Tavi P (2011) 
# Impact of Sarcoplasmic Reticulum Calcium Release on Calcium Dynamics and
# Action Potential Morphology in Human Atrial Myocytes: A Computational Study.
# PLoS Comput Biol 7(1): e1001067.
# http://dx.doi.org/10.1371/journal.pcbi.1001067
#
# - Model developed based on:
#   * Nygren et al. (1998) model, including
#   * updated K+ current formulations as in Maleckar et al. (2009)
#
#**************************************************************************

#********************************************
# Cell Geometry
#********************************************

parameters(# Cell geometry
           lcell = ScalarParam(122.051, unit="um", description="Length of cell"),
           rjunct = ScalarParam(6.5, unit="um", description="Radius of bulk cytosol"),
           dr = ScalarParam(1.625, unit="um", description="nSR width of the bulk cytosol compartments"),
           tpore = ScalarParam(0.5, description="Transport-available porosity"),
           SRratio = ScalarParam(0.0225, description="Volume ratio of bulk cytosol that is SR"),
           Vss = ScalarParam(4.99232e-5, unit="nL", description="Volume of subspace"))

# Transport-available volume of SR compartments, unit: nL
V1 = pi*dr*dr*lcell*tpore * 1e-6 
V2 = 3*V1              
V3 = 5*V1              
V4 = 7*V1              

# Transport-available volume of SR compartments, unit: nL
VSR1 = SRratio * V1 
VSR2 = SRratio * V2
VSR3 = SRratio * V3
VSR4 = SRratio * V4

# Total transport-available non-junctional/total cytosolic volume, unit: nL
Vbulk = 16*V1 
Vcytosol = Vbulk + Vss

# Transport-available interface between junctional and bulk cytosol
Aj_nj = 2*pi*rjunct*lcell*tpore 
# Diffusion distance from center of junct to first njunct
xj_nj = 0.02/2 + dr/2
# Diffusion distance from center of junct to center of njunct 
xj_nj_Nai = 0.02/2 + 2*dr

#********************************************
# Physical & environmental constants
#********************************************

parameters(Cm = ScalarParam(0.05, unit="nF", description="Cell membrane capacitance"),
           F = ScalarParam(96487, unit="C*mole**-1", description="Faraday constant"),
           R = ScalarParam(8314, unit="mJ*mole**-1*K**-1", description="Ideal gas constant"),
           T = ScalarParam(310.15, unit="K", description="Temperature"),
           Nao = ScalarParam(130, unit="mM", description="Extracellular Na"),
           Cao = ScalarParam(1.8, unit="mM", description="Extracellular Ca"),
           Ko  = ScalarParam(5.4, unit="mM", description="Extracellular K"))

# Q10 temperature parameters
parameters(q10_DCaNa = 1.18,
           q10_DCaBmSR = 1.425, # Sidell & Hazel (1987)
           q10_INa = 3, # Benndorf (1994)
           q10_Ito = 2.6, # Radicke et al. (2013): 2.4/2.8 for act/inact
           q10_IKur = 2.2,
           q10_SERCA = 2,
           q10_RyR = 1.5,
           q10_NKA = 1.63, 
           q10_CaP = 2.35,
           q10_NCX = 1.57)

q10exp = (T - 310.15)/10

states (V = ScalarParam(-74.0726, unit="mM", description="Membrane potential"),
        Nai = ScalarParam(8.4573, unit="mM", description="Intracellular Na"),
        Nass = ScalarParam(8.3024, unit="mM", description="Subspace Na"),  
        Ki = ScalarParam(136.4182, unit="mM", description="Intracellular K"),  
        Cass = ScalarParam(1.6717e-04, unit="mM", description="Subspace Ca"))

# Reversal potentials, from Nernst equation
ENa = R*T/F * log(Nao/Nass)
EK  = R*T/F * log(Ko/Ki)
ECa = R*T/F/2 * log(Cao/Cass)

#********************************************
# Sodium Current, INa (New model)
#********************************************

states(m = ScalarParam(0.0076, description="Activiation of INa"),
       h1 = ScalarParam(0.7659, description="Inactivation of INaF"),
       h2 = ScalarParam(0.7665, description="Inactivation of INaL"),
       f1 = ScalarParam(0.5976, description="Inactivation of INaL"))

parameters(gNa = ScalarParam(0.558, unit="uS", description="Max cond."),
           gNaL = ScalarParam(0.000423, unit="uS", description="Max cond.")) 

# Q10 temperature scaling
m_shift = -4.65 * q10exp # linear T-dependent shift
h_shift = -7.85 * q10exp # linear T-dependent shift, not including Yuan et al.

# Steady-states
m_inf = 1/(1 + exp(-(V+39+m_shift)/7.2))
h_inf = 1/(1 + exp((V+67+h_shift)/6.0))
f1_inf = 1/(1 + exp((V+72+h_shift)/5.1))

# Time constants, unit: ms
tau_m  = (1/q10_INa**q10exp) * (0.01 + 0.13*exp(-((V+48+m_shift)/15)**2) + 0.045/(1+exp(-(V+42+m_shift)/5)))
tau_h1 = (1/q10_INa**q10exp) * (0.07 + 34/(1 + exp((V+41+h_shift)/5.5) + exp(-(V+41+h_shift)/14)) + 0.2/(1+exp(-(V+79+m_shift)/14)))
tau_h2 = (1/q10_INa**q10exp) * (0.7  + 150/(1 + exp((V+41+h_shift)/5.5) + exp(-(V+41+h_shift)/14)) + 2/(1+exp(-(V+79+h_shift)/14)))
tau_f1 = (1/q10_INa**q10exp) * 200 # O'Hara et al.

# Currents, unit: nA
INaF = gNa * m**3 * h1 * h2 * (V - ENa)
INaL = gNaL * m**3 * f1 * (V - ENa)
INa = INaF + INaL

# Differential Equations
dm_dt  = (m_inf - m)/tau_m
dh1_dt = (h_inf - h1)/tau_h1    
dh2_dt = (h_inf - h2)/tau_h2   
df1_dt = (f1_inf - f1)/tau_f1

#********************************************
# L-type Calcium, ICaL
#********************************************

parameters(gCaL = ScalarParam(0.007, unit="uS", description="Maximum conductance"),
           kCa = ScalarParam(0.6e-3, unit="mM", description="Half-max binding concentration"))

# Apparent reversal potential
ECa_app = 60 + 29.2*log(Cao/1.8)/log(10) # Campbell et al. (1988)

states(d = ScalarParam(8.5787e-05, description="Voltage dep. activation gate of ICaL"),
       f2 = ScalarParam(0.9971, description="Voltage dep. inactivation gate of ICaL"),
       fca = ScalarParam(0.7821, description="Calcium dep. inactivation gate of ICaL"))

# Steady-states
d_inf = 1/(1 + exp(-(V+9.5)/6.9))
f2_inf = 0.04 + 0.96/(1 + exp((V + 25.5)/8.4)) + 1/(1 + exp(-(V-60)/8.0)) # fit Li et al. data
fca_inf = 1/(1 + (Cass/kCa)) # Courtemanche with larger kCa

# Time Constants, unit: ms
tau_d = 0.65 * exp(-((V + 35)/30.)**2) + 0.5 # Nygren four-fold faster, according to Cavalie
tau_f2 = 1340*exp(-((V+40)/14.2)**2) + 40 # average from Li et al. and Christ et al. (55 + 51.8 + 20.6 + 33.9)/4
tau_fca = 2

# Current, unit: nA
ICaL = gCaL * d * f2 * fca * (V - ECa_app) # Nygren without f1

# Differential equations
dd_dt = (d_inf - d)/tau_d
df2_dt = (f2_inf - f2)/tau_f2
dfca_dt = (fca_inf - fca)/tau_fca

#********************************************
# Transient Outward Current, It
#********************************************

states(tr = ScalarParam(0.0011, description="Activation gate of It"),
       ts = ScalarParam(0.9490, description="Inactivation gate of It"))

parameters(gt = ScalarParam(0.011, unit="uS", description="Max conductance of It")) # Increased ~33% from Maleckar

# Steady-states
tr_inf = 1/(1 + exp(-(V-1)/11)) # Nygren
ts_inf = 1/(1 + exp((V+40.5)/11.5)) # Nygren

# Time constants, unit: ms
tau_tr = (1/q10_Ito**q10exp) * (2.4*exp(-((V+0)/30)**2) + 1) # Nygren at 37C
tau_ts = (1/q10_Ito**q10exp) * (18*exp(-((V+52.45)/15.88)**2) + 9.6) # Maleckar at 37C

# Current, unit: nA
It = gt * tr * ts * (V - EK)

# Differential Equations
dtr_dt = (tr_inf - tr)/tau_tr
dts_dt = (ts_inf - ts)/tau_ts

#*************************************************
# Delayed Outward Rectifiers IKr, IKs, IKur, IK1, 
#*************************************************

parameters(gKr = ScalarParam(0.0034, unit="uS", description="Max conductance"), 
           gKs = ScalarParam(0.000175, unit="uS", description="Max conductance"), # Grandi
           gKur = ScalarParam(0.00225, unit="uS", description="Max conductance")) # Maleckar

states(pa = ScalarParam(0.0013, description="Activation gate of Kr"),
       n = ScalarParam(0.0071, description="Activation gate of Ks"),
       r = ScalarParam(3.6296e-04, description="Activation gate of Kur"),
       s = ScalarParam(0.9666, description="Inactivation gate of Kur"))

# Steady-states
pa_inf = 1/(1 + exp(-(V+10)/5))
n_inf = 1/(1 + exp(-(V+3.8)/14.25)) # Grandi
r_inf = 1/(1 + exp(-(V+6)/8.6)) # Maleckar et al.
s_inf = 1/(1 + exp((V+7.5)/10)) # Maleckar et al.

# Time Constants, unit: ms
tau_pa = 550/(1 + exp(-(V+22)/9)) * 6/(1 + exp((V+11)/9)) + 230/(1 + exp((V+40)/20)) 
tau_n = 990.1/(1 + exp(-(V+2.436)/14.12))
tau_r = (1/q10_IKur**q10exp) * (6.6/(1 + exp((V+5)/12)) + 0.36) # Maleckar at 37C
tau_s = (1/q10_IKur**q10exp) * (430/(1 + exp((V+60)/10)) + 2200) # Maleckar at 37C

# Currents, unit: nA
IKr = gKr * sqrt(Ko/5.4) * pa * 1/(1+exp((V+74)/24)) * (V - EK) # Grandi
IKs = gKs * n**2 * (V - EK)
IKur = gKur * r * s * (V - EK)

# Differential equations
dpa_dt = (pa_inf - pa)/tau_pa
dn_dt = (n_inf - n)/tau_n
dr_dt = (r_inf - r)/tau_r
ds_dt = (s_inf - s)/tau_s

#*************************************************
# Inward Rectifier Current IK1
#*************************************************
           
parameters(gK1 = ScalarParam(0.0029, unit="uS", description="Max conductance of IK1"))

# Current, unit: nA
IK1 = gK1 * Ko**0.4457 * (V - EK) / (1 + exp(1.5*(V-EK+3.6)*F/R/T))

#*************************************************
# Background currents, INab, ICab
#*************************************************

parameters(gNab = ScalarParam(6.0599e-5, unit="us", description="Constant cond."),
           gCab = ScalarParam(8.4e-5, unit="uS", description="Constant cond.")) 

# Leak currents, unit: nA
INab = gNab * (V - ENa)
ICab = gCab * (V - ECa)

#********************************************
# Exchanger and Pump Currents
#********************************************

parameters(INaKmax = ScalarParam(0.113, unit="nA", description="Max INaK current"),
           ICaPmax = ScalarParam(0.002, unit="nA", description="Max ICaP current"),
           INaCamax = ScalarParam(0.001, unit="nA", description="Max INaCa current"),
           KmKo = ScalarParam(1.5, unit="mM", description="Half-Maximum Na binding concentration for INaK"),
           KmNaip = ScalarParam(11, unit="mM", description="Half-Maximum Na binding concentration for INaK"),
           kCaP = ScalarParam(0.0005, unit="mM", description="Half-Maximum Ca binding concentration for ICaP"),
           kNaCa = ScalarParam(0.0088, unit="nA/mM**4", description="Scaling factor for INaCa"),
           gam = ScalarParam(0.45, description="Relative position of barrier controlling INaCa V dependence"),
           dNaCa = ScalarParam(0.0003, unit="mM**-4", description="Denominator constant of INaCa"))

# INaK, unit: nA (Grandi et al)
sigma = (exp(Nao/67.3)-1)/7 
fNaK = 1/(1 + 0.1245*exp(-0.1*V*F/R/T) + 0.0365*sigma*exp(-V*F/R/T))
INaK = INaKmax * q10_NKA**q10exp * fNaK * Ko /(1+(KmNaip/Nass)**4)/(Ko+KmKo)

# INaCa, unit: nA
INaCa = INaCamax * kNaCa * ((exp(gam*V*F/R/T)*Nass**3*Cao - exp((gam-1)*V*F/R/T)*Nao**3*Cass)/(1 + dNaCa*(Nao**3*Cass + Nass**3*Cao)))

# ICaP, unit: nA
ICaP = ICaPmax * Cass/(kCaP+Cass)

#********************************************
# Funny current, If (Zorn-Pauly LAW fit)
#********************************************

states(y = ScalarParam(0.05500, description="Activation gate for If"))

parameters(gIf = ScalarParam(0.001, unit="nS", description="Max conductance of If"),
           Na_fratio = ScalarParam(0.2677, description="Na-ratio of total funny current"))

# Steady state
y_inf = 1/(1 + exp((V+97.8274)/12.48025))

# Time constant: ms
tau_y = 1000/(0.00332*exp(-V/16.54103) + 23.71839*exp(V/16.54103))

# Currents, unit: nA
IfNa = gIf * y * Na_fratio * (V - ENa)
IfK  = gIf * y * (1 - Na_fratio) * (V - EK)
If = IfK + IfNa

# Differential Equations
dy_dt = (y_inf - y)/tau_y

#********************************************
# SK Channel 
#********************************************

states(O = ScalarParam(0.0918, description="Steady-state for IKCa"))
 
parameters(gKCa = ScalarParam(3.7e-3, unit="nS", description="Max conductance of IKCa"),
           alpha_KCa = ScalarParam(47e3, unit="ms**-1*mM**-2", description="Activation rate for IKCa"),
           beta_KCa = ScalarParam(13e-3, unit="ms**-1", description="Inactivation rate for IKCa"))

# rectification based on Tao et al. (2015)
IKCa = gKCa * O * (1/(1 + exp((V-EK+120)/45))) * (V-EK)

# Rates from Hirschberg et al (1998)
dO_dt = alpha_KCa * Cass**2 * (1 - O) - beta_KCa * O

#********************************************
# Stimulus Current
#********************************************

parameters(PCL = ScalarParam(1000, unit="ms", description="Cycle length"),
           stim_duration = ScalarParam(2, unit="ms", description="Duration of stim pulse"),
           stim_amplitude = ScalarParam(-1.25*0.460, unit="nA", description="Magnitude of stimulus"),
           stim_offset = ScalarParam(0, unit="ms", description="Time-shift in cycles"))

past = floor(time/PCL)*PCL

# Current: nA, carried by K
Istim = Conditional(And(Ge(time-past, stim_offset), Le(time-past, stim_offset + stim_duration), ), stim_amplitude, 0)

#********************************************
# Internal Calcium Dynamics
#********************************************

states(# Cytosolic concentrations
       Cai1 = ScalarParam(1.7481e-04, unit="mM"),
       Cai2 = ScalarParam(1.7351e-04, unit="mM"),
       Cai3 = ScalarParam(1.7139e-04, unit="mM"),
       Cai4 = ScalarParam(1.6857e-04, unit="mM"),

       # SR concentrations
       CaSR1 = ScalarParam(0.6310, unit="mM"),
       CaSR2 = ScalarParam(0.6261, unit="mM"),
       CaSR3 = ScalarParam(0.6198, unit="mM"),
       CaSR4 = ScalarParam(0.6159, unit="mM"))

#********************************************
# RyR
#********************************************

states(RyRoss = ScalarParam(7.983e-5, description="Activation of RyR"),
       RyRo1 = ScalarParam(2.8060e-04, description="Activation of RyR"),
       RyRo2 = ScalarParam(2.2525e-04, description="Activation of RyR"),
       RyRo3 = ScalarParam(1.5452e-04, description="Activation of RyR"),

       RyRcss = ScalarParam(0.9998, description="Inactivation of RyR"),
       RyRc1 = ScalarParam(0.9912, description="Inactivation of RyR"),
       RyRc2 = ScalarParam(0.9956, description="Inactivation of RyR"),
       RyRc3 = ScalarParam(0.9986, description="Inactivation of RyR"),

       RyRass = ScalarParam(0.2298, unit="uM", description="Adaptation of RyR"),
       RyRa1 = ScalarParam(0.1995, unit="uM", description="Adaptation of RyR"),
       RyRa2 = ScalarParam(0.2047, unit="uM", description="Adaptation of RyR"),
       RyRa3 = ScalarParam(0.2136, unit="uM", description="Adaptation of RyR"))

parameters(RyRtauact = ScalarParam(16, unit="ms", description="RyR activation time constant"),
           RyRtauactss = ScalarParam(4.3, unit="ms", description="RyR activation time constant in SS"),
           RyRtauinact = ScalarParam(74, unit="ms", description="RyR inactivation time constant "),
           RyRtauinactss = ScalarParam(13, unit="ms", description="RyR inactivation time constant in SS"),
           RyRtauadapt = ScalarParam(850, unit="ms", description="RyR adaptation time constant"),
           RyRmax = ScalarParam(0.0016, unit="ms**-1/nL", description="Max RyR flux"),
           RyRmaxss = ScalarParam(0.900, unit="ms**-1/nL", description="Max RyR flux in SS"))

# Steady-states
RyRoss_inf = 1 - 1/(1 + exp((Cass*1000-(RyRass+0.22))/0.03))
RyRo1_inf = 1 - 1/(1 + exp((Cai1*1000-(RyRa1+0.22))/0.03))
RyRo2_inf = 1 - 1/(1 + exp((Cai2*1000-(RyRa2+0.22))/0.03))
RyRo3_inf = 1 - 1/(1 + exp((Cai3*1000-(RyRa3+0.22))/0.03))
RyRcss_inf = 1/(1 + exp((Cass*1000-(RyRass+0.02))/0.01))
RyRc1_inf = 1/(1 + exp((Cai1*1000-(RyRa1+0.02))/0.01))
RyRc2_inf = 1/(1 + exp((Cai2*1000-(RyRa2+0.02))/0.01))
RyRc3_inf = 1/(1 + exp((Cai3*1000-(RyRa3+0.02))/0.01))
# Adaptation unit: uM
RyRass_inf = 0.505-0.427/(1 + exp((Cass*1000-0.29)/0.082))
RyRa1_inf = 0.505-0.427/(1 + exp((Cai1*1000-0.29)/0.082))
RyRa2_inf =  0.505-0.427/(1 + exp((Cai2*1000-0.29)/0.082))
RyRa3_inf =  0.505-0.427/(1 + exp((Cai3*1000-0.29)/0.082))

# SR load scaling
RyRSRCass = 1 - 1/(1+exp((CaSR4-0.3)/0.1))
RyRSRCa1  = 1 - 1/(1+exp((CaSR1-0.3)/0.1))
RyRSRCa2  = 1 - 1/(1+exp((CaSR2-0.3)/0.1))
RyRSRCa3  = 1 - 1/(1+exp((CaSR3-0.3)/0.1))

# Temperature scaling
tau_RyRadapt   = (1/q10_RyR**q10exp) * RyRtauadapt  
tau_RyRactss   = (1/q10_RyR**q10exp) * RyRtauactss  
tau_RyRinactss = (1/q10_RyR**q10exp) * RyRtauinactss
tau_RyRact     = (1/q10_RyR**q10exp) * RyRtauact    
tau_RyRinact   = (1/q10_RyR**q10exp) * RyRtauinact  

# Release currents, unit: nL*mM/s 
Jrelss = RyRmaxss * Vss * RyRoss * RyRcss * RyRSRCass * (CaSR4 - Cass)
Jrel1  = RyRmax   * V1  * RyRo1  * RyRc1  * RyRSRCa1  * (CaSR1 - Cai1)
Jrel2  = RyRmax   * V2  * RyRo2  * RyRc2  * RyRSRCa2  * (CaSR2 - Cai2)
Jrel3  = RyRmax   * V3  * RyRo3  * RyRc3  * RyRSRCa3  * (CaSR3 - Cai3)

# Derivatives
dRyRoss_dt = (RyRoss_inf - RyRoss)/tau_RyRactss
dRyRo1_dt  = (RyRo1_inf - RyRo1)/tau_RyRact
dRyRo2_dt  = (RyRo2_inf - RyRo2)/tau_RyRact
dRyRo3_dt  = (RyRo3_inf - RyRo3)/tau_RyRact
dRyRcss_dt = (RyRcss_inf - RyRcss)/tau_RyRinactss
dRyRc1_dt  = (RyRc1_inf - RyRc1)/tau_RyRinact
dRyRc2_dt  = (RyRc2_inf - RyRc2)/tau_RyRinact
dRyRc3_dt  = (RyRc3_inf - RyRc3)/tau_RyRinact
dRyRass_dt = (RyRass_inf - RyRass)/tau_RyRadapt
dRyRa1_dt  = (RyRa1_inf - RyRa1)/tau_RyRadapt
dRyRa2_dt  = (RyRa2_inf - RyRa2)/tau_RyRadapt
dRyRa3_dt  = (RyRa3_inf - RyRa3)/tau_RyRadapt

#********************************************
# SR Leak current
#********************************************

parameters(kSRleak = ScalarParam(6e-6, unit="ms**-1", description="kSRleak "))

#**** Leak Currents nl*mmol*ms**-1 **************
JSRleak1 = kSRleak * (CaSR1 - Cai1) * V1
JSRleak2 = kSRleak * (CaSR2 - Cai2) * V2
JSRleak3 = kSRleak * (CaSR3 - Cai3) * V3
JSRleakss = kSRleak * (CaSR4 - Cass) * Vss

#********************************************
# SERCA pumps
#********************************************

states(SERCACa1  = ScalarParam(0.0061, description="Concentration of SERCA-bound calcium", unit="mM"),
       SERCACa2  = ScalarParam(0.0061, description="Concentration of SERCA-bound calcium", unit="mM"),
       SERCACa3  = ScalarParam(0.0059, description="Concentration of SERCA-bound calcium", unit="mM"),
       SERCACass = ScalarParam(0.0058, description="Concentration of SERCA-bound calcium", unit="mM"))

parameters(cpumps = ScalarParam(30e-3, unit="mM", description="Concentration of pumps in cytosol volume"),
           phos = ScalarParam(0.1, description="Baseline phosphorylation"),
           k4base = ScalarParam(0.017, unit="ms**-1", description="Pump rate baseline"),
           Kmf_PLBKO = ScalarParam(0.15e-3, unit="mM", description="Phospholamban Knockout forward"),
           Kmr_PLBKO = ScalarParam(2.5, unit="mM", description="Phospholamban Knockout reverse"),
           Kmf_PLB = ScalarParam(0.12e-3, unit="mM", description="Phospholamban forward"),
           Kmr_PLB = ScalarParam(0.88, unit="mM", description="Phospholamban reverse"),
           Kmf_SLN = ScalarParam(0.07e-3, unit="mM", description="Sarcolipin forward"),
           Kmr_SLN = ScalarParam(0.5, unit="mM", description="Sarcolipin reverse"))

# Binding affinities, unit: mM
SERCAKmf = Kmf_PLBKO + Kmf_PLB * (1 - phos) + Kmf_SLN * (1 - phos)
SERCAKmr = Kmr_PLBKO - Kmr_PLB * (1 - phos) - Kmr_SLN * (1 - phos)

# Pump rates
k4 = k4base * q10_SERCA**q10exp # unit: ms**-1
k1 = 1000**2 * k4 # unit: ms**-1 mM**-2
k2 = k1 * SERCAKmf**2 # unit: ms**-1
k3 = k4/SERCAKmr**2 # unit: ms**-1 mM**-2

# Fluxes, unit: nL*mM*ms**-1
JSERCASR1  = (-k3*CaSR1**2*(cpumps-SERCACa1)  + k4*SERCACa1)*V1*2
JSERCASR2  = (-k3*CaSR2**2*(cpumps-SERCACa2)  + k4*SERCACa2)*V2*2  
JSERCASR3  = (-k3*CaSR3**2*(cpumps-SERCACa3)  + k4*SERCACa3)*V3*2
JSERCASRss = (-k3*CaSR4**2*(cpumps-SERCACass) + k4*SERCACass)*Vss*2 

# Fluxes, unit: nL*mM*ms**-1
JbulkSERCA1  = (k1*Cai1**2*(cpumps-SERCACa1)  - k2*SERCACa1)*V1*2
JbulkSERCA2  = (k1*Cai2**2*(cpumps-SERCACa2)  - k2*SERCACa2)*V2*2
JbulkSERCA3  = (k1*Cai3**2*(cpumps-SERCACa3)  - k2*SERCACa3)*V3*2
JbulkSERCAss = (k1*Cass**2*(cpumps-SERCACass) - k2*SERCACass)*Vss*2

dSERCACa1_dt  = 0.5*(JbulkSERCA1  - JSERCASR1)/V1
dSERCACa2_dt  = 0.5*(JbulkSERCA2  - JSERCASR2)/V2
dSERCACa3_dt  = 0.5*(JbulkSERCA3  - JSERCASR3)/V3
dSERCACass_dt = 0.5*(JbulkSERCAss - JSERCASRss)/Vss

#********************************************
# Ca buffering and diffusion
#********************************************

parameters(DCabase   = ScalarParam(0.833, unit="um/ms", description="Free diffusion coefficient for Ca"),
           DCaSRbase = ScalarParam(0.0507, unit="um/ms", description="Free diffusion coefficient for Ca in SR"),
           DCaBmbase = ScalarParam(0.0288, unit="um/ms", description="Free diffusion coefficient for Ca-buffer complex"),
           BCa = ScalarParam(0.024, unit="uM", description="Total arbitrary calcium buffer concentration"), 
           SLlow = ScalarParam(165., unit="mM", description="Phospolipid concentration (low-affinity sites)"), 
           SLhigh = ScalarParam(13., unit="mM", description="Phospolipid concentration (high-affinity sites)"), 
           KdBCa = ScalarParam(0.00238, unit="mM", description="Dissociation constant for arbitrary cytosol Ca buffer"), 
           KdSLlow = ScalarParam(1.1, unit="mM", description="Dissociation constant for low-affinity phospholipid sites"), 
           KdSLhigh = ScalarParam(0.013, unit="mM", description="Dissociation constant for high-affinity phospholipid sites"), 
           CSQN = ScalarParam(6.7, unit="mM", description="Concentration of Calsequestrin"), 
           KdCSQN = ScalarParam(0.8, unit="mM", description="Dissociation constant for calsequestrin"))

betass = (1 + SLlow*KdSLlow/(Cass + KdSLlow)**2 + SLhigh*KdSLhigh/(Cass + KdSLhigh)**2 + BCa*KdBCa/(Cass + KdBCa)**2)**(-1)
gammai1 = BCa*KdBCa/(Cai1 + KdBCa)**2
gammai2 = BCa*KdBCa/(Cai2 + KdBCa)**2
gammai3 = BCa*KdBCa/(Cai3 + KdBCa)**2
gammai4 = BCa*KdBCa/(Cai4 + KdBCa)**2
betai1  = 1/(1 + gammai1)
betai2  = 1/(1 + gammai2)
betai3  = 1/(1 + gammai3)
betai4  = 1/(1 + gammai4)
betaSR1 = 1/(1 + CSQN*KdCSQN/(CaSR1 + KdCSQN)**2)
betaSR2 = 1/(1 + CSQN*KdCSQN/(CaSR2 + KdCSQN)**2)
betaSR3 = 1/(1 + CSQN*KdCSQN/(CaSR3 + KdCSQN)**2)
betaSR4 = 1/(1 + CSQN*KdCSQN/(CaSR4 + KdCSQN)**2)

# Temperature scaling
DCa   = DCabase * q10_DCaNa**q10exp
DCaSR = DCaSRbase * q10_DCaBmSR**q10exp
DCaBm = DCaBmbase * q10_DCaBmSR**q10exp

# Currents, unit: mM*ms**-1
JdiffSR1 = betaSR1 * DCaSR * (3*CaSR2 -  3*CaSR1)          /(2*dr**2)
JdiffSR2 = betaSR2 * DCaSR * (5*CaSR3 -  8*CaSR2 + 3*CaSR1)/(4*dr**2)
JdiffSR3 = betaSR3 * DCaSR * (7*CaSR4 - 12*CaSR3 + 5*CaSR2)/(6*dr**2)
JdiffSR4 = betaSR4 * DCaSR *         (-  7*CaSR4 + 7*CaSR3)/(8*dr**2)

Jdiff1 = betai1*(DCa+gammai1*DCaBm)*(3*Cai2- 3*Cai1)       /(2*dr**2) - 2*betai1*gammai1*DCaBm/(KdBCa+Cai1)*((Cai2-Cai1)/2/dr)**2
Jdiff2 = betai2*(DCa+gammai2*DCaBm)*(5*Cai3- 8*Cai2+3*Cai1)/(4*dr**2) - 2*betai2*gammai2*DCaBm/(KdBCa+Cai2)*((Cai3-Cai1)/2/dr)**2
Jdiff3 = betai3*(DCa+gammai3*DCaBm)*(7*Cai4-12*Cai3+5*Cai2)/(6*dr**2) - 2*betai3*gammai3*DCaBm/(KdBCa+Cai3)*((Cai4-Cai2)/2/dr)**2
Jdiff4 = betai4*(DCa+gammai4*DCaBm)*      (- 7*Cai4+7*Cai3)/(8*dr**2) - 2*betai4*gammai4*DCaBm/(KdBCa+Cai4)*((Cai4-Cai3)/2/dr)**2

# Unit: mmol*ms**-1
Jdiffss = DCa*Aj_nj/xj_nj*(Cass-Cai4) * 1e-6

#********************************************
# SR Calcium
#********************************************

JSRCa1 = JSERCASR1 - JSRleak1 - Jrel1
JSRCa2 = JSERCASR2 - JSRleak2 - Jrel2
JSRCa3 = JSERCASR3 - JSRleak3 - Jrel3
JSRCa4 = JSERCASRss - JSRleakss - Jrelss

dCaSR1_dt = JdiffSR1 + betaSR1*JSRCa1/VSR1
dCaSR2_dt = JdiffSR2 + betaSR2*JSRCa2/VSR2 
dCaSR3_dt = JdiffSR3 + betaSR3*JSRCa3/VSR3 
dCaSR4_dt = JdiffSR4 + betaSR4*JSRCa4/VSR4 

#********************************************
# Cytosolic Calcium
#********************************************

JCa1 = -JbulkSERCA1 + JSRleak1 + Jrel1
JCa2 = -JbulkSERCA2 + JSRleak2 + Jrel2
JCa3 = -JbulkSERCA3 + JSRleak3 + Jrel3
JCa4 = Jdiffss
JCass = -Jdiffss + JSRleakss - JbulkSERCAss + Jrelss

dCai1_dt = Jdiff1 + JCa1/V1*betai1
dCai2_dt = Jdiff2 + JCa2/V2*betai2
dCai3_dt = Jdiff3 + JCa3/V3*betai3
dCai4_dt = Jdiff4 + JCa4/V4*betai4

#********************************************
# Sodium diffusion and buffering
#********************************************

parameters(DNa = ScalarParam(0.146e-3, unit="um*s**-1", description="Free diffusion coefficient of sodium"),
           BNa = ScalarParam(1.1319, unit="mM"),
           KdBNa = ScalarParam(10, unit="mM"))

betaNass = (1 + BNa*KdBNa/(Nass + KdBNa)**2)**(-1)

# Unit: mmol*ms**-1
JNa = DNa * q10_DCaNa**q10exp * Aj_nj / xj_nj_Nai * (Nass - Nai)* 1e-6

#********************************************
# Fibroblast Model --- Active I
#********************************************

states(Vf = ScalarParam(-74.101968, unit="mV", description="Fibroblast Membrane Potential"),
       Kf = ScalarParam(129.116327, unit="mM", description="Fibroblast Intracellular Potassium Concentration"),
       Naf = ScalarParam(8.949972, unit="mM", description="Fibroblast Intracellular Sodium Concentration"),
       rf = ScalarParam(0.007234, description="Activation gate for fibroblast time and voltage dependant K+ current"),
       sf = ScalarParam(0.976399, description="Inactivation gate for fibroblast time and voltage dependant K+ current"))

parameters(Cf = ScalarParam(0.0063, unit="nF"),
           Vcytosolf = ScalarParam(0.00137, unit="nL", description="Fibroblast Intracellular volume"),
           gKvf = ScalarParam(0.250, unit="nS/pF", description="Max conductance of fibroblast time and voltage dependent potassium current"),
           gK1f = ScalarParam(0.4822, unit="nS/pF", description="Max conductance of fibroblast inward rectifier current"),
           gNabf = ScalarParam(0.0095, unit="nS/pF", description="Conductance of fibroblast background sodium current"),
           INaKmaxf = ScalarParam(1.644, unit="pA/pF", description="Max current of fibroblast sodium-potassium exchanger"),
           VrevNaKf = ScalarParam(-150.0, unit="mV", description="Reversial potential for fibroblast sodium-potassium exchanger"),
           B = ScalarParam(-200.0, unit="mV", description="Scaling Parameter for fibroblast sodium-potassium exchanger"),
           KmKf = ScalarParam(1.0 , unit="mM", description="Binding constant for fibroblast sodium-potassium exchanger"),
           KmNaf = ScalarParam(11.0, unit="mM", description="Binding constant for fibroblast sodium-potassium exchanger"),
           nfib = ScalarParam(1, description="Number of coupled fibroblasts"),
           Gmf = ScalarParam(0.0005, unit="uS", description="Gap junction conductance"))

# Nernst Potentials
EKf   =  R*T/F * log(Ko/Kf)
ENaf  =  R*T/F * log(Nao/Naf)

# Time and voltage dependant K current, unit: nA/pF
iKvf = gKvf * rf * sf * (Vf-EKf)
rf_inf = 1/(1 + exp(-(Vf + 20)/11))
sf_inf = 1/(1 + exp((Vf + 23)/7))
tau_rf =  20.3 + 138.0*exp(-((Vf+20)/25.9)**2)
tau_sf =  1574. + 5268.*exp(-((Vf+23)/22.7)**2)
drf_dt = (rf_inf-rf)/tau_rf
dsf_dt = (sf_inf-sf)/tau_sf

# Inward-rectifier K current density, unit: nA/pF
alpha_K1f = 0.1/(1 + exp(0.06*(Vf - EKf - 200.0)))
beta_K1f = (3*exp(0.0002*(Vf-EKf+100.0))+exp(0.1*(Vf-EKf-10.0)))/(1.0+exp(-0.5*(Vf-EKf)))
iK1f = (gK1f*alpha_K1f*(Vf-EKf))/(alpha_K1f+beta_K1f)

# Sodium-potassium Exchanger current density, unit: nA/pF
iNaKf = INaKmaxf*(Ko/(Ko+KmKf))*((pow(Naf,1.5))/((pow(Naf,1.5))+(pow(KmNaf,1.5))))*(Vf-VrevNaKf)/(Vf-B)

# Background sodium current density, unit: nA/pF
iNabf = gNabf*(Vf - ENaf)

# Myocyte-fibroblast gap junction current, unit: nA
Igap = Gmf*(V - Vf)

# Change in membrane potential
dVf_dt = -(iKvf + iK1f + iNabf + iNaKf) + Igap/(Cf*1e-3)

# Change in ion concentrations
dKf_dt = -(iK1f + iKvf - 2*iNaKf)*(1e3*Cf)/(Vcytosolf*F)
dNaf_dt = -(iNabf + 3*iNaKf)*(1e3*Cf)/(Vcytosolf*F)

#********************************************
# Membrane potential and ion concentrations
#********************************************

# Change in ion concentrations
dV_dt = -(INa + ICaL + It + IKur + IK1 + IKr + IKs + INab + ICab + INaK + ICaP + INaCa + If + IKCa + Istim + nfib*Igap)/Cm

dNai_dt = JNa/Vbulk
dNass_dt = betaNass*(-JNa/Vss - (INa + INab + 3*INaK + 3*INaCa + IfNa) / (Vss*F))
dKi_dt = -(It + IKur + IK1 + IKr + IKs - 2*INaK + IfK + IKCa + Istim)/(Vcytosol*F)
dCass_dt = betass*(JCass/Vss + (-ICaL - ICab - ICaP + 2*INaCa) / (2*Vss*F))

