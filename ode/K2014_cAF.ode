#**************************************************************************
# Koivumaki human Atrial Myocyte model - 20154
#
# This code was adapted to gotran-form for M.Sc. thesis work by 
# Jonas van den Brink based on the original authors MATLAB implementation
# 
# Several simplifcations have been made
# - We only include square-shaped stimulus
#**************************************************************************

#**************************************************************************
# Human atrial myocyte model --- Chronic Atrial Fibrillation
#
# Citation:
# Koivumaki JT, Korhonen T, Tavi P (2011) 
# Impact of Sarcoplasmic Reticulum Calcium Release on Calcium Dynamics and
# Action Potential Morphology in Human Atrial Myocytes: A Computational Study.
# PLoS Comput Biol 7(1): e1001067.
# http://dx.doi.org/10.1371/journal.pcbi.1001067
#
# Koivumaki JT, Seemann G, Maleckar MM, Tavi P (2014)
# In Silico Screening of the Key Cellular Remodeling Targets in
# Chronic Atrial Fibrillation. 
# PLoS Comput Biol 10(5): e1003620. 
# doi:10.1371/journal.pcbi.1003620
#
#**************************************************************************

#********************************************
# Model Variant
#********************************************

parameters(cAF_lcell = ScalarParam(1.10, description="Elongation of cell conductance"),
           cAF_gCaL = ScalarParam(0.41, description="Downreg of max ICaL conductance"),
           cAF_gt = ScalarParam(0.38, description="Upreg of max It conductance"),
           cAF_gKur = ScalarParam(0.62, description="Downreg of max IKur"),
           cAF_gK1 = ScalarParam(1.62, description="Upreg of max IK1 conductance"),
           cAF_kNaCa = ScalarParam(1.50, description="Upreg of max INaCa conductance"),
           cAF_cpumps = ScalarParam(0.84, description="Reduction in cpump concentration"),
           cAF_PLB = ScalarParam(1.18, description="Increased Phospholamban-to-SERCA ratio"),
           cAF_SLN = ScalarParam(0.60, description="Increased Sarcolipin-to-SERCA ratio"),
           cAF_phos = ScalarParam(2, description="Increase in phosphorylation"),
           cAF_RyR = ScalarParam(2, description="Increased sensitivity of RyR to SR Ca"))

#********************************************
# Cell Geometry
#********************************************

parameters(# Cell geometry
           lcellbase = ScalarParam(122.051, unit="um", description="Length of cell"),
           rjunctbase = ScalarParam(6.5, unit="um", description="Radius of bulk cytosol"),
           drbase = ScalarParam(1.625, unit="um", description="nSR width of the bulk cytosol compartments"),
           tpore = ScalarParam(0.5, description="Transport-available porosity"),
           SRratio = ScalarParam(0.0225, description="Volume ratio of bulk cytosol that is SR"),
           Vssbase = ScalarParam(4.99232e-5, unit="nL", description="Volume of subspace"))

# Cell dilation in cAF
Ddcell = 1 + (cAF_lcell - 1) * (20/10)
Dvcell = cAF_lcell * Ddcell**2
Vss = Vssbase * Dvcell
dr = drbase * Ddcell
lcell = lcellbase * cAF_lcell
rjunct = rjunctbase * Ddcell

# Transport-available volume of SR compartments, unit: nL
V1 = pi*dr*dr*lcell*tpore * 1e-6 
V2 = 3*V1              
V3 = 5*V1              
V4 = 7*V1              

# Transport-available volume of SR compartments, unit: nL
VSR1 = SRratio * V1 / Dvcell
VSR2 = SRratio * V2 / Dvcell
VSR3 = SRratio * V3 / Dvcell
VSR4 = SRratio * V4 / Dvcell

# Total transport-available non-junctional/total cytosolic volume, unit: nL
Vbulk = 16*V1 
Vcytosol = Vbulk + Vss

# Transport-available interface between junctional and bulk cytosol
Aj_nj = 2*pi*rjunct*lcell*tpore 
# Diffusion distance from center of junct to first njunct
xj_nj = 0.02/2*Ddcell + dr/2
# Diffusion distance from center of junct to center of njunct 
xj_nj_Nai = 0.02/2*Ddcell + 2*dr

#********************************************
# Physical & environmental constants
#********************************************

parameters(Cm = ScalarParam(0.05, unit="nF", description="Cell membrane capacitance"),
           F = ScalarParam(96487, unit="C*mole**-1", description="Faraday constant"),
           R = ScalarParam(8314, unit="mJ*mole**-1*K**-1", description="Ideal gas constant"),
           T = ScalarParam(306.15, unit="K", description="Temperature"),
           Nao = ScalarParam(130, unit="mM", description="Extracellular Na"),
           Cao = ScalarParam(1.8, unit="mM", description="Extracellular Ca"),
           Ko  = ScalarParam(5.4, unit="mM", description="Extracellular K"))

states (V = ScalarParam(-7.847950e+01, unit="mM", description="Membrane potential"),
        Nai = ScalarParam(9.033266e+00, unit="mM", description="Intracellular Na"),
        Nass = ScalarParam(8.993138e+00, unit="mM", description="Subspace Na"),  
        Ki = ScalarParam(1.381715e+02, unit="mM", description="Intracellular K"),  
        Cass = ScalarParam(1.453511e-04, unit="mM", description="Subspace Ca"))

# Reversal potentials, from Nernst equation
ENa = R*T/F * log(Nao/Nass)
EK  = R*T/F * log(Ko/Ki)
ECa = R*T/F/2 * log(Cao/Cass)

#********************************************
# Sodium Current, INa
#********************************************

states(m = ScalarParam(1.915751e-03, description="Activation gate for INa"),
       h1 = ScalarParam(9.434007e-01, description="Fast inactivation gate for INa"),
       h2 = ScalarParam(9.420716e-01, description="Slow inactivation gate for INa"))

parameters(PNa = ScalarParam(0.00182e-3, unit="nL*s**-1", description="Permeability of INa"))

# Activation functions
m_inf = 1/(1 + exp((V+27.12)/-8.21))
h_inf = 1/(1 + exp((V+63.6)/5.3))

# Time constants
tau_m = 0.024 + 0.042*exp(-((V+25.57)/28.8)**2)
tau_h1 = 0.3 + 30.0/(1+exp((V+35.1)/3.2))
tau_h2 = 3.0 + 120.0/(1+exp((V+35.1)/3.2))

# Sodium current
INa = PNa * m**3 * (0.9*h1+0.1*h2) * Nao * V * F**2/(R*T) * (exp((V-ENa)*F/R/T)-1)/(exp(V*F/R/T)-1)

# Derivatives
dm_dt  = (m_inf - m)/tau_m
dh1_dt = (h_inf - h1)/tau_h1    
dh2_dt = (h_inf - h2)/tau_h2   

#********************************************
# L-type Calcium, ICaL
#********************************************

parameters(gCaL = ScalarParam(0.015, unit="nS", description="Maximum conductance"),
           kCa = ScalarParam(0.6e-3, unit="mM", description="Half-maxmum binding concentration"))

# Apparent reversal potential
ECa_app = 60 + 29.2*log(Cao/1.8)/log(10) # Campbell et al. (1988)

states(d = ScalarParam(6.272437e-06, description="Voltage dep. activation gate of ICaL"),
       f2 = ScalarParam(9.982601e-01, description="Voltage dep. inactivation gate of ICaL"),
       fca = ScalarParam(9.445483e-01, description="Calcium dep. inactivation gate"))

# Steady-states
d_inf = 1/(1 + exp((V+9)/-5.8))
f2_inf = 0.04 + 0.96/(1 + exp((V+25.5)/8.4)) + 1/(1 + exp(-(V-60)/8.0))
fca_inf = 1/(1 + (Cass/kCa)**2)

# Time Constants, unit: ms
tau_d = 0.5 + 0.65*exp(-((V+35.0)/30.0)**2)
tau_f2 = 40.0 + 1340.0*exp(-((V+40)/14.2)**2)
tau_fca = 2.0

# Current, unit: nA
ICaL = gCaL * cAF_gCaL * d * f2 * fca * (V - ECa_app)

# Differential equations
dd_dt = (d_inf - d)/tau_d
df2_dt = (f2_inf - f2)/tau_f2
dfca_dt = (fca_inf - fca)/tau_fca

#********************************************
# Transient Outward Current, It
#********************************************

states(tr = ScalarParam(7.272109e-04, description="Activation gate of It"),
       ts = ScalarParam(9.645691e-01, description="Inactivation gate of It"))

parameters(gt = ScalarParam(0.00825, unit="nS", description="Max conductance of It"))

# Steady-states
tr_inf = 1/(1+exp((V-1)/-11))
ts_inf = 1/(1+exp((V+40.5)/11.5))

# Time constants, unit: ms
tau_tr = 1.5 + 3.5*exp(-((V+0)/30)**2)
# ts_tau = 14.14 + 481.2*exp(-((V+52.45)/14.97)**2) # Original
tau_ts = 14.14 + 25.635*exp(-((V+52.45)/15.8827)**2) # Maleckar et al.

# Current, unit: nA
It = gt * cAF_gt * tr * ts * (V - EK)

# Differential Equations
dtr_dt = (tr_inf - tr)/tau_tr
dts_dt = (ts_inf - ts)/tau_ts

#*************************************************
# Delayed Outward Rectifiers IKr, IKs, IKur
#*************************************************

states(r = ScalarParam(2.183718e-04, description="Activation gate of Kur"),
       s = ScalarParam(9.473402e-01, description="Inactivation gate of Kur"),
       n = ScalarParam(7.781858e-03, description="Activation gate of Ks"),
       pa = ScalarParam(2.528123e-05, description="Activation gate of Kr"))

parameters(gKr = ScalarParam(0.0005, unit="uS", description="Max conductance"), 
           gKs = ScalarParam(0.001, unit="uS", description="Max conductance"), # Grandi
           gKur = ScalarParam(0.00225, unit="uS", description="Max conductance")) # Maleckar

# Steady-states
r_inf =  1/(1 + exp((V+6)/-8.6)) # Maleckar et al.
s_inf =  1/(1 + exp((V+7.5)/10)) # Maleckar et al.
n_inf =  1/(1 + exp((V-19.9)/-12.7))
pa_inf = 1/(1 + exp((V+15)/-6))
p_i = 1/(1+exp((V+55)/24))

# Time Constants, unit: ms
tau_r = 0.5 + 9.0/(1 + exp((V+5)/12)) # Maleckar et al.
tau_s = 3050. + 590.0/(1 + exp((V + 60)/10)) # Maleckar et al.
tau_n = 700.0 + 400.0*exp(-((V-20)/20)**2)
tau_pa = 31.18 + 217.18*exp(-((V+20.1376)/22.1996)**2)


# Currents, unit: nA
IKr  = gKr * pa * p_i * (V - EK)
IKs  = gKs * n  * (V - EK)
IKur = gKur * cAF_gKur * r   * s * (V - EK)

# Differential equations
dpa_dt = (pa_inf - pa)/tau_pa
dn_dt = (n_inf - n)/tau_n
dr_dt = (r_inf - r)/tau_r
ds_dt = (s_inf - s)/tau_s

#*************************************************
# Inward Rectifier Current IK1
#*************************************************
           
parameters(gK1 = ScalarParam(0.0035, unit="uS", description="Max conductance of IK1"))

# Current, unit: nA
IK1  = gK1 * cAF_gK1 * Ko**0.4457 * (V - EK) / (1+exp(1.5*(V-EK+3.6)*F/R/T))

#*************************************************
# Background currents, INab, ICab
#*************************************************

parameters(gNab = ScalarParam(0.000060599, description="Conductance of Nab"),
           gCab = ScalarParam(0.0000952, description="Conductance of Cab"))

# Leak currents, unit: nA
INab = gNab * (V - ENa)
ICab = gCab * (V - ECa)

#********************************************
# Exchanger and Pump Currents
#********************************************

parameters(INaKmax = ScalarParam(0.0708253, unit="nl*s**-1", description="Max INaK current"),
           kNaKK = ScalarParam(1.0, unit="mM"), 
           kNaKNa = ScalarParam(11.0, unit="mM"), 
           ICaPmax = ScalarParam(0.002, unit="nl*s**-1", description="Max ICaP current"),
           kCaP = ScalarParam(0.0005, unit="mM"), 
           kNaCa = ScalarParam(0.0084e-3, unit="mM"), 
           dNaCa = 0.0003, 
           gam = 0.45, 
           fCaNCX = 1) 

# INaK
INaK = INaKmax * Ko/(Ko+kNaKK) * Nass**1.5/(Nass**1.5+kNaKNa**1.5) * (V+150)/(V+200)

# INaCa
INaCa = kNaCa * cAF_kNaCa * ((exp(gam*V*F/R/T)*Nass**3*Cao - exp((gam-1)*V*F/R/T)*Nao**3*Cass*fCaNCX)/(1 + dNaCa*(Nao**3*Cass*fCaNCX + Nass**3*Cao)))

# ICaP
ICaP = ICaPmax*Cass/(kCaP+Cass)

#********************************************
# Funny current, If (Zorn-Pauly LAW fit)
#********************************************

states(y = ScalarParam(6.449382e-02, description="Activation gate for If"))

parameters(gIf = ScalarParam(0.001, unit="nS", description="Max conductance of If"),
           Na_fratio = ScalarParam(0.2677, description="Na-ratio of total funny current"))

# Steady state
y_inf = 1/(1 + exp((V+97.82874)/12.48025))

# Time constant: ms
tau_y = 1000.0/(0.00332*exp(-V/16.54103) + 23.71839*exp(V/16.54103))

IfNa = gIf * y * Na_fratio * (V - ENa)
IfNa = gIf * y * (0.2677)  * (V - ENa)
IfK  = gIf * y * (1 - Na_fratio) * (V - EK)
If = IfK + IfNa

dy_dt = (y_inf - y)/tau_y

#********************************************
# Stimulus Current
#********************************************

parameters(stim_duration = ScalarParam(2, unit="ms", description="Duration of stim pulse"),
           PCL = ScalarParam(1000, unit="ms", description="Cycle length"),
           stim_amplitude = ScalarParam(-1.25*0.675, unit="uA", description="Magnitude of stimulus"),
           stim_offset = ScalarParam(0, unit="ms", description="Time-shift in cycles"))

past = floor(time/PCL)*PCL
Istim = Conditional(And(Ge(time - past, stim_offset), Le(time - past, stim_offset + stim_duration), ), stim_amplitude*cAF_gK1, 0) # nA

#********************************************
# Internal Calcium Dynamics
#********************************************

states(# Cytosolic concentrations
       Cai1 = ScalarParam(1.192527e-04, unit="mM"),
       Cai2 = ScalarParam(1.213926e-04, unit="mM"),
       Cai3 = ScalarParam(1.269560e-04, unit="mM"),
       Cai4 = ScalarParam(1.394698e-04, unit="mM"),

       # SR concentrations
       CaSR1 = ScalarParam(3.326372e-01, unit="mM"),
       CaSR2 = ScalarParam(3.181425e-01, unit="mM"),
       CaSR3 = ScalarParam(2.902650e-01, unit="mM"),
       CaSR4 = ScalarParam(2.529017e-01, unit="mM"))

#********************************************
# RyR
#********************************************

states(RyRoss = 2.009899e-03,
       RyRo1 = 5.832926e-03,
       RyRo2 = 5.243616e-03,
       RyRo3 = 3.932382e-03,

       RyRcss = 9.999377e-01,
       RyRc1 = 9.977725e-01,
       RyRc2 = 9.982581e-01,
       RyRc3 = 9.990238e-01,

       RyRass = 2.212457e-01,
       RyRa1 = 1.630763e-01,
       RyRa2 = 1.683496e-01,
       RyRa3 = 1.823765e-01)

parameters(RyRtauact = ScalarParam(18.75, unit="ms", description="RyR activation time constant"),
           RyRtauactss = ScalarParam(5, unit="ms", description="RyR activation time constant in SS"),
           RyRtauinact = ScalarParam(87.5, unit="ms", description="RyR inactivation time constant "),
           RyRtauinactss = ScalarParam(15, unit="ms", description="RyR inactivation time constant in SS"),
           RyRtauadapt = ScalarParam(1000, unit="ms", description="RyR adaptation time constant"),
           RyRmax = ScalarParam(1e-3, unit="ms**-1/nL", description="Max RyR flux"),
           RyRmaxss = ScalarParam(0.625, unit="ms**-1/nL", description="Max RyR flux in SS"))

# Steady-states
RyRoss_inf = 1 - 1/(1 + exp((Cass*1000-(RyRass+0.22/cAF_RyR))/0.03))
RyRo1_inf = 1 - 1/(1 + exp((Cai1*1000-(RyRa1+0.22/cAF_RyR))/0.03))
RyRo2_inf = 1 - 1/(1 + exp((Cai2*1000-(RyRa2+0.22/cAF_RyR))/0.03))
RyRo3_inf = 1 - 1/(1 + exp((Cai3*1000-(RyRa3+0.22/cAF_RyR))/0.03))
RyRcss_inf = 1/(1 + exp((Cass*1000-(RyRass+0.02))/0.01))
RyRc1_inf = 1/(1 + exp((Cai1*1000-(RyRa1+0.02))/0.01))
RyRc2_inf = 1/(1 + exp((Cai2*1000-(RyRa2+0.02))/0.01))
RyRc3_inf = 1/(1 + exp((Cai3*1000-(RyRa3+0.02))/0.01))
# Adaptation unit: uM
RyRass_inf = 0.505-0.427/(1 + exp((Cass*1000-0.29)/0.082))
RyRa1_inf = 0.505-0.427/(1 + exp((Cai1*1000-0.29)/0.082))
RyRa2_inf =  0.505-0.427/(1 + exp((Cai2*1000-0.29)/0.082))
RyRa3_inf =  0.505-0.427/(1 + exp((Cai3*1000-0.29)/0.082))

# SR load scaling
RyRSRCass = 1 - 1/(1+exp((CaSR4-0.3/cAF_RyR)/0.1))
RyRSRCa1  = 1 - 1/(1+exp((CaSR1-0.3/cAF_RyR)/0.1))
RyRSRCa2  = 1 - 1/(1+exp((CaSR2-0.3/cAF_RyR)/0.1))
RyRSRCa3  = 1 - 1/(1+exp((CaSR3-0.3/cAF_RyR)/0.1))

# Release currents, unit: nL*mM/s 
Jrelss = RyRmaxss * Vss * RyRoss * RyRcss * RyRSRCass * (CaSR4 - Cass) / Dvcell
Jrel1  = RyRmax   * V1  * RyRo1  * RyRc1  * RyRSRCa1  * (CaSR1 - Cai1) / Dvcell
Jrel2  = RyRmax   * V2  * RyRo2  * RyRc2  * RyRSRCa2  * (CaSR2 - Cai2) / Dvcell
Jrel3  = RyRmax   * V3  * RyRo3  * RyRc3  * RyRSRCa3  * (CaSR3 - Cai3) / Dvcell

# Derivatives
dRyRoss_dt = (RyRoss_inf - RyRoss)/RyRtauactss
dRyRo1_dt  = (RyRo1_inf  - RyRo1) /RyRtauact
dRyRo2_dt  = (RyRo2_inf  - RyRo2) /RyRtauact
dRyRo3_dt  = (RyRo3_inf  - RyRo3) /RyRtauact
dRyRcss_dt = (RyRcss_inf - RyRcss)/RyRtauinactss
dRyRc1_dt  = (RyRc1_inf - RyRc1) /RyRtauinact
dRyRc2_dt  = (RyRc2_inf  - RyRc2) /RyRtauinact
dRyRc3_dt  = (RyRc3_inf  - RyRc3) /RyRtauinact
dRyRass_dt = (RyRass_inf - RyRass)/RyRtauadapt
dRyRa1_dt  = (RyRa1_inf - RyRa1) /RyRtauadapt
dRyRa2_dt  = (RyRa2_inf  - RyRa2) /RyRtauadapt
dRyRa3_dt  = (RyRa3_inf  - RyRa3) /RyRtauadapt

#********************************************
# SR Leak current
#********************************************

parameters(kSRleak = ScalarParam(6e-6, unit="ms**-1", description="kSRleak "))

#**** Leak Currents nl*mmol*ms**-1 **************
JSRleak1 = kSRleak * (CaSR1 - Cai1) * V1 / Dvcell
JSRleak2 = kSRleak * (CaSR2 - Cai2) * V2 / Dvcell
JSRleak3 = kSRleak * (CaSR3 - Cai3) * V3 / Dvcell
JSRleakss = kSRleak * (CaSR4 - Cass) * Vss / Dvcell

#********************************************
# SERCA pumps
#********************************************

states(SERCACa1  = ScalarParam(9.545045e-04, unit="mM"),
       SERCACa2  = ScalarParam(9.012774e-04, unit="mM"),
       SERCACa3  = ScalarParam(8.056255e-04, unit="mM"),
       SERCACass = ScalarParam(7.291065e-04, unit="mM"))

parameters(cpumpbase = ScalarParam(30e-3, unit="mM", description="Concentration of pumps in cytosol volume"),
           base_phos = ScalarParam(0.1, description="Baseline phosphorylation"),
           k4base = ScalarParam(0.013, unit="ms**-1", description="Pump rate baseline"),
           Kmf_PLBKO = ScalarParam(0.15e-3, unit="mM", description="Phospholamban Knockout forward"),
           Kmr_PLBKO = ScalarParam(2.5, unit="mM", description="Phospholamban Knockout reverse"),
           Kmf_PLB = ScalarParam(0.12e-3, unit="mM", description="Phospholamban forward"),
           Kmr_PLB = ScalarParam(0.88, unit="mM", description="Phospholamban reverse"),
           Kmf_SLN = ScalarParam(0.07e-3, unit="mM", description="Sarcolipin forward"),
           Kmr_SLN = ScalarParam(0.5, unit="mM", description="Sarcolipin reverse"))

# Binding affinities, unit: mM
phos = base_phos * cAF_phos
PLB_SERCA_ratio = cAF_PLB
SLN_SERCA_ratio = cAF_SLN

SERCAKmf = Kmf_PLBKO + Kmf_PLB * PLB_SERCA_ratio * (1 - phos) + Kmf_SLN * SLN_SERCA_ratio * (1 - phos)
SERCAKmr = Kmr_PLBKO - Kmr_PLB * PLB_SERCA_ratio * (1 - phos) - Kmr_SLN * SLN_SERCA_ratio * (1 - phos)

# Pump rates
cpumps = cpumpbase*cAF_cpumps/Dvcell
k4 = k4base # unit: ms**-1
k1 = 1000**2 * k4 # unit: ms**-1 mM**-2
k2 = k1 * SERCAKmf**2 # unit: ms**-1
k3 = k4/SERCAKmr**2 # unit: ms**-1 mM**-2

# Fluxes, unit: nL*mM*ms**-1
JSERCASR1  = (-k3*CaSR1**2*(cpumps-SERCACa1)  + k4*SERCACa1)*V1*2
JSERCASR2  = (-k3*CaSR2**2*(cpumps-SERCACa2)  + k4*SERCACa2)*V2*2  
JSERCASR3  = (-k3*CaSR3**2*(cpumps-SERCACa3)  + k4*SERCACa3)*V3*2
JSERCASRss = (-k3*CaSR4**2*(cpumps-SERCACass) + k4*SERCACass)*Vss*2 

# Fluxes, unit: nL*mM*ms**-1
JbulkSERCA1  = (k1*Cai1**2*(cpumps-SERCACa1)  - k2*SERCACa1)*V1*2
JbulkSERCA2  = (k1*Cai2**2*(cpumps-SERCACa2)  - k2*SERCACa2)*V2*2
JbulkSERCA3  = (k1*Cai3**2*(cpumps-SERCACa3)  - k2*SERCACa3)*V3*2
JbulkSERCAss = (k1*Cass**2*(cpumps-SERCACass) - k2*SERCACass)*Vss*2

dSERCACa1_dt  = 0.5*(JbulkSERCA1  - JSERCASR1)/V1
dSERCACa2_dt  = 0.5*(JbulkSERCA2  - JSERCASR2)/V2
dSERCACa3_dt  = 0.5*(JbulkSERCA3  - JSERCASR3)/V3
dSERCACass_dt = 0.5*(JbulkSERCAss - JSERCASRss)/Vss

#********************************************
# Ca buffering and diffusion
#********************************************

parameters(DCa   = ScalarParam(0.780, unit="um/ms", description="Free diffusion coefficient for Ca"),
           DCaSR = ScalarParam(0.044, unit="um/ms", description="Free diffusion coefficient for Ca in SR"),
           DCaBm = ScalarParam(0.025, unit="um/ms", description="Free diffusion coefficient for Ca-buffer complex"),
           BCa = ScalarParam(0.024, unit="uM", description="Total arbitrary calcium buffer concentration"), 
           SLlow = ScalarParam(165., unit="mM", description="Phospolipid concentration (low-affinity sites)"), 
           SLhigh = ScalarParam(13., unit="mM", description="Phospolipid concentration (high-affinity sites)"), 
           KdBCa = ScalarParam(0.00238, unit="mM", description="Dissociation constant for arbitrary cytosol Ca buffer"), 
           KdSLlow = ScalarParam(1.1, unit="mM", description="Dissociation constant for low-affinity phospholipid sites"), 
           KdSLhigh = ScalarParam(0.013, unit="mM", description="Dissociation constant for high-affinity phospholipid sites"), 
           CSQN = ScalarParam(6.7, unit="mM", description="Concentration of Calsequestrin"), 
           KdCSQN = ScalarParam(0.8, unit="mM", description="Dissociation constant for calsequestrin"))

betass = (1 + SLlow*KdSLlow/(Cass + KdSLlow)**2 + SLhigh*KdSLhigh/(Cass + KdSLhigh)**2 + BCa*KdBCa/(Cass + KdBCa)**2)**(-1)
gammai1 = BCa*KdBCa/(Cai1 + KdBCa)**2
gammai2 = BCa*KdBCa/(Cai2 + KdBCa)**2
gammai3 = BCa*KdBCa/(Cai3 + KdBCa)**2
gammai4 = BCa*KdBCa/(Cai4 + KdBCa)**2
betai1  = 1/(1 + gammai1)
betai2  = 1/(1 + gammai2)
betai3  = 1/(1 + gammai3)
betai4  = 1/(1 + gammai4)
betaSR1 = 1/(1 + CSQN*KdCSQN/(CaSR1 + KdCSQN)**2)
betaSR2 = 1/(1 + CSQN*KdCSQN/(CaSR2 + KdCSQN)**2)
betaSR3 = 1/(1 + CSQN*KdCSQN/(CaSR3 + KdCSQN)**2)
betaSR4 = 1/(1 + CSQN*KdCSQN/(CaSR4 + KdCSQN)**2)

# Currents, unit: mM*ms**-1
JdiffSR1 = betaSR1 * DCaSR * (3*CaSR2 -  3*CaSR1)          /(2*dr**2)
JdiffSR2 = betaSR2 * DCaSR * (5*CaSR3 -  8*CaSR2 + 3*CaSR1)/(4*dr**2)
JdiffSR3 = betaSR3 * DCaSR * (7*CaSR4 - 12*CaSR3 + 5*CaSR2)/(6*dr**2)
JdiffSR4 = betaSR4 * DCaSR *         (-  7*CaSR4 + 7*CaSR3)/(8*dr**2)

Jdiff1 = betai1*(DCa+gammai1*DCaBm)*(3*Cai2- 3*Cai1)       /(2*dr**2) - 2*betai1*gammai1*DCaBm/(KdBCa+Cai1)*((Cai2-Cai1)/2/dr)**2
Jdiff2 = betai2*(DCa+gammai2*DCaBm)*(5*Cai3- 8*Cai2+3*Cai1)/(4*dr**2) - 2*betai2*gammai2*DCaBm/(KdBCa+Cai2)*((Cai3-Cai1)/2/dr)**2
Jdiff3 = betai3*(DCa+gammai3*DCaBm)*(7*Cai4-12*Cai3+5*Cai2)/(6*dr**2) - 2*betai3*gammai3*DCaBm/(KdBCa+Cai3)*((Cai4-Cai2)/2/dr)**2
Jdiff4 = betai4*(DCa+gammai4*DCaBm)*      (- 7*Cai4+7*Cai3)/(8*dr**2) - 2*betai4*gammai4*DCaBm/(KdBCa+Cai4)*((Cai4-Cai3)/2/dr)**2

# Unit: mmol*ms**-1
Jdiffss = DCa*Aj_nj/xj_nj*(Cass-Cai4) * 1e-6

#********************************************
# SR Calcium
#********************************************

JSRCa1 = JSERCASR1 - JSRleak1 - Jrel1
JSRCa2 = JSERCASR2 - JSRleak2 - Jrel2
JSRCa3 = JSERCASR3 - JSRleak3 - Jrel3
JSRCa4 = JSERCASRss - JSRleakss - Jrelss

dCaSR1_dt = JdiffSR1 + betaSR1*JSRCa1/VSR1
dCaSR2_dt = JdiffSR2 + betaSR2*JSRCa2/VSR2 
dCaSR3_dt = JdiffSR3 + betaSR3*JSRCa3/VSR3 
dCaSR4_dt = JdiffSR4 + betaSR4*JSRCa4/VSR4 

#********************************************
# Cytosolic Calcium
#********************************************

JCa1 = -JbulkSERCA1 + JSRleak1 + Jrel1
JCa2 = -JbulkSERCA2 + JSRleak2 + Jrel2
JCa3 = -JbulkSERCA3 + JSRleak3 + Jrel3
JCa4 = Jdiffss
JCass = -Jdiffss + JSRleakss - JbulkSERCAss + Jrelss

dCai1_dt = Jdiff1 + JCa1/V1*betai1
dCai2_dt = Jdiff2 + JCa2/V2*betai2
dCai3_dt = Jdiff3 + JCa3/V3*betai3
dCai4_dt = Jdiff4 + JCa4/V4*betai4

#********************************************
# Sodium diffusion and buffering
#********************************************

parameters(DNa = ScalarParam(0.00012, unit="um*s**-1", description="Free diffusion coefficient of sodium"),
           BNa = ScalarParam(1.1319, unit="mM"),
           KdBNa = ScalarParam(10, unit="mM"))

betaNass = (1 + BNa*KdBNa/(Nass + KdBNa)**2)**(-1)

# Unit: mmol*ms**-1
JNa = DNa * Aj_nj / xj_nj_Nai * (Nass - Nai)* 1e-6

#********************************************
# Membrane potential and ion concentrations
#********************************************

# Change in ion concentrations
dV_dt = -(INa + ICaL + It + IKur + IK1 + IKr + IKs + INab + ICab + INaK + ICaP + INaCa + If + Istim)/Cm

dNai_dt = JNa/Vbulk
dNass_dt = betaNass*(-JNa/Vss - (INa + INab + 3*INaK + 3*INaCa + IfNa) / (Vss*F))
dKi_dt = -(It + IKur + IK1 + IKr + IKs - 2*INaK + IfK + Istim)/(Vcytosol*F)
dCass_dt = betass*(JCass/Vss + (-ICaL - ICab - ICaP + 2*INaCa) / (2*Vss*F))

