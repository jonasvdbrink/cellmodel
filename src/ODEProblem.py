# -*- coding: utf-8 -*-

from gotran.codegeneration import compile_module
from gotran.common.options import parameters
from gotran.model.loadmodel import load_ode
import numpy as np
import matplotlib.pyplot as plt

class ODEProblem(object):
    """
    Class for compiling a ODE system defined in gotran to C and
    create high-level methods to interface the resulting solvers
    """
    def __init__(self, ode, solver="explicit_euler", intermediates=[],
                 im=[], odepath="../ode/", initcondpath="../data/initcond/",
                 jacobian=False):
        """
        Compile a gotran .ode file to C and set up 
        the module and corresponding forward methods
        """
        generation = parameters.generation.copy()
        
        # We don't want to compile unnecessary features
        for what_not in ["componentwise_rhs_evaluation",
                         "forward_backward_subst",
                         "linearized_rhs_evaluation",
                         "lu_factorization",
                         "jacobian"]:
            generation.functions[what_not].generate = False

        # Select what numerical scheme we want the solver to use
        if solver == 'forward_euler': solver = 'explicit_euler'
        generation.solvers[solver].generate = True

        if jacobian: generation.functions["jacobian"].generate = True
        
        # Store names and paths for later use
        self.ode = ode 
        self.odepath = odepath
        self.initcondpath = initcondpath + "%s/" % self.ode

        # Load ode from file
        if isinstance(ode, str):
            ode = load_ode(odepath + ode)
        
        if isinstance(intermediates, str): intermediates = [intermediates]
        if isinstance(im, str): im = [im]
        intermediates = list(intermediates) + list(im)
        if intermediates == 'all':
            self.intermediates = [i.name for i in ode.intermediates]
        else:
            self.intermediates = intermediates
        
        # Compile ode
        self.module = compile_module(ode, "C", self.intermediates, generation)
        self.forward = getattr(self.module, "forward_"+solver)

        # Set up jacobian
        if jacobian:
            self.jacobian = getattr(self.module, "compute_jacobian")
        else: 
            self.jacobian = None

        # Set up all indicies
        self.__find_indices(ode)

        # Set initial conditions
        self.reset_model()
        if intermediates:
            self.intermediates = self.module.monitor(self.states,
                                                     self.t, self.parameters)
        
    def __find_indices(self, ode):
        """Sets up index dictionaries for later use"""
        self.state_indices = {}
        self.param_indices = {}
        self.intermediate_indices = {}
        for s in ode.states:
            self.state_indices[s.name] = self.module.state_indices(s.name)
        for p in ode.parameters:      
            self.param_indices[p.name] = self.module.parameter_indices(p.name)
        for j, i in enumerate(self.intermediates):
            self.intermediate_indices[i] = j
        
    def reset_model(self):
        """Reset model to start"""
        self.reset_parameters()
        self.reset_states()
        self.t = 0

    def reset_states(self):
        """Set states to initial values specified in ODE model"""
        self.states = self.module.init_state_values()

    def reset_parameters(self):
        """Set parameters to those specified in ODE model"""
        self.parameters = self.module.init_parameter_values()

    def reset_time(self):
        """Set the current model time to 0"""
        self.t = 0

    def get_state(self, state):
        """Returns current value of a statee"""
        try:
            return self.states[self.state_indices[state]]        
        except KeyError:
            raise KeyError("State %s not found in model %s" % (state, self.ode))

    def get_states(self, states):
        """Returns current value of a statee"""
        if states == 'all':
            states = self.list_states()
        elif not isinstance(states, list):
            raise TypeError("States must be given as list")

        return {s : self.get_state(s) for s in states}    


    def set_state(self, state, value):
        """Set model state to given value"""
        try:
            self.states[self.state_indices[state]] = value
        except KeyError:
            raise KeyError("State %s not found in model %s" % (state, self.ode))

    def save_states(self, filename):
        """Save the current states to a .npy file"""
        np.save(filename, self.states)

    def load_states(self, filename):
        try:
            self.states = np.load(filename + ".npy")
        except:
            e = "Cannot load states from file %s" % (filename)
            raise ValueError(e)

    def get_param(self, param):
        """Returns current value of a parameter"""
        try:
            return self.parameters[self.param_indices[param]]
        except KeyError:
            raise KeyError("Parameter %s not found in model %s" % (param, self.ode))

    def set_param(self, param, value):
        """Set model parameter to given value"""
        try:
            self.parameters[self.param_indices[param]] = value
        except KeyError:
            raise KeyError("Param %s not found in model %s" % (param, self.ode))

    def set_params(self, params, values):   
        """Set several parameters at once"""
        if not len(params) == len(values):
            e = "Parameter and value lists must be of equal length."
            raise ValueError(e)
        for i in range(len(params)):
            self.set_param(params[i], values[i])

    def get_time(self):
        """Return the current model time"""
        return self.t

    def set_time(self, t):
        """Set the current model time"""
        self.t = t

    def get_intermediate(self, intermediate):
        """Get the current value of a monitored intermediate state"""
        try:
            self.module.monitor(self.states, self.t, self.parameters,
                                self.intermediates)
        except:
            e = "Intermediate %s not found in compiled ODE." % intermediate
            raise KeyError(e)
        return self.intermediates[self.intermediate_indices[intermediate]]

    def list_states(self):
        """Return list of all state names"""
        return self.state_indices.keys()

    def list_parameters(self):
        """Return list of all parameter names"""
        return self.param_indices.keys()

    def list_intermediates(self):
        """Return list of all intermediates"""
        return self.intermediate_indices.keys()

    def time(self):
        """Return current time"""
        return self.t

    def step(self, dt):
        """Takes one time step of size dt"""
        self.forward(self.states, self.t, dt, self.parameters)
        self.t += dt

    def solve(self, dt, T):
        """Solve until time T in steps of dt"""
        while self.t < T:
            self.step(dt)

    def clampsolve(self):
        raise NotImplementedError

    def tracksolve(self, dt, T, states=[], intermediates=[], im=[], clamp=[]):
        """
        Solve until time T in steps of dt, keep track 
        of specified states and intermediates, return
        tracked data as dictionary.
        """
        if isinstance(intermediates, str): intermediates = [intermediates]
        if isinstance(im, str): im = [im]
        intermediates = list(intermediates) + list(im)
        if self.t >= T:
            e = "End time must after current model time."
            raise ValueError(e)
        if clamp: 
            self.clampsolve(dt, T, states, intermediates, clamp)
        if not states and not intermediates:
            self.solve(dt, T)
            return
        t0 = self.get_time()
        N = int(np.ceil((T-t0)/dt))
        if states == 'all':
            states = self.list_states()
        elif isinstance(states, str):
            states = [states]
        if not isinstance(states, list):
            e = "states not understood. Must be single str or list of strs."
            raise KeyError(e)
        
        if intermediates == 'all':
            intermediates = self.list_intermediates()
        elif isinstance(intermediates, str):
            intermediates = [intermediates]
        if not isinstance(intermediates, list):
            e = "intermediates not understood. Must be single str/list of strs."
            raise KeyError(e)
        
        tracked = {s : np.zeros(N+1) for s in states+intermediates}
        # Initial values
        for s in states:
            tracked[s][0] = self.get_state(s)
        for i in intermediates:
            tracked[i][0] = self.get_intermediate(i)
        # Solve and track
        for j in xrange(N):
            self.step(dt)
            for s in states:
                tracked[s][j+1] = self.get_state(s)
            for i in intermediates:
                tracked[i][j+1] = self.get_intermediate(i)
        t = np.linspace(t0, self.time(), N+1)
        return t, tracked

    def compute_jacobian(self):
        """Return Jacobian based current states, time, and parameters"""
        if not self.jacobian:
            e = "ODE has not been compiled with jacobian flag. Please recompile"
            raise StandardError(e)
        return self.jacobian(self.states, self.time(), self.parameters)

    def compute_eigenvalues(self):
        """Return eigenvalues of the Jacobian of the system"""
        J = self.compute_jacobian()
        w, v = np.linalg.eig(J)
        return w


if __name__ == '__main__':
    solver = ODEProblem('exp_decay')
    t, d = solver.tracksolve(dt=1.2, T=5.3, states='u')
    plt.plot(t, d['u'], 'o-')
    plt.show()