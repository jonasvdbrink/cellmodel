from ODEProblem import ODEProblem
import numpy as np
import matplotlib.pyplot as plt

class CellModel(ODEProblem):
    """
    Class for compiling an electrophysiological cell model defined in gotran
    to C and create high-level methods to interface the resulting solvers
    """
    def steadycycle(self, dt, T, filename='', load=True):
        if load and filename:
            try:
                self.load_states(self.initcondpath + filename)
                self.reset_time()
                return
            except:
                if load == "force":
                    raise IOError("File %s not found." % filename)
        self.solve(dt, T)
        if filename: self.save_states(self.initcondpath + filename)
        self.reset_time()
        return

if __name__ == '__main__':
    K2015 = "K2015_nSR"
    fe = CellModel(K2015, solver='explicit_euler', im=['INa', 'ICaL'])
    t, d = fe.tracksolve(dt=0.01, T=1000, states="V", im=['INa', 'ICaL'])

    plt.subplot(3,1,1)
    plt.plot(t, d['V'])
    plt.ylabel('V')
    plt.axis([0, 500, -80, 40])
    plt.subplot(3,1,2)
    plt.plot(t, d['INa'])
    plt.ylabel('INa')
    plt.axis([0, 500, -0.25, 0])
    plt.subplot(3,1,3)
    plt.plot(t, d['ICaL'])
    plt.ylabel('ICaL')
    plt.axis([0, 500, -0.25, 0])
    plt.xlabel('Time [ms]')
    plt.show()