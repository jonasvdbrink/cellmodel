from CellModel import CellModel
import matplotlib.pyplot as plt
import numpy as np

class MyocyteModel(CellModel):
    """
    Subclass of CellModel more specialized for myocyte models, expects
    there to be a PCL parameter for the stimulus as well as a V state.
    """
    def __init__(self, *args, **kwargs):
        super(MyocyteModel, self).__init__(*args, **kwargs)
        if not "V" in self.list_states():
            e = "'V' missing. MyocyteModel requires a state 'V'."
            raise KeyError(e)
        if not "PCL" in self.list_parameters():
            e = "'PCL' missing. MyocyteModel requires a parameter 'PCL'."
            raise KeyError(e)

    def get_PCL(self):
        """Returns current basic cycle length of model"""
        return self.get_param("PCL")

    def get_BCL(self):
        """Handle different naming conventions"""
        return self.get_PCL()

    def set_PCL(self, PCL):
        """Sets basic cycle length of model"""
        return self.set_param("PCL", PCL)

    def set_BCL(self, BCL):
        """Handle different naming conventions"""
        return self.set_PCL(BCL)

    @property
    def V(self):
        """Return current membrane potential""" 
        return self.get_state("V")

    @V.setter
    def V(self, value):
        self.set_state('V', value)

    def action_potential(self, dt):
        """Solve 1 action potential of model."""
        self.reset_time()
        t, d = self.tracksolve(dt, T=self.get_PCL(), states='V')
        return t, d['V']

    @staticmethod
    def find_APD(t, V, repolarization=0.9):
        """
        Take time and potential arrays, extract and return APD
        at given level of repolarization.
        """
        dt = t[1]-t[0]
        Vmin = np.min(V); Vmax = np.max(V)
        Vthresh = (Vmax - Vmin)*(1 - repolarization) + Vmin
        APD = len(V[V > Vthresh])*dt
        return APD


if __name__ == '__main__':
    solver = MyocyteModel("K2015_nSR", solver='explicit_euler')
    t, V = solver.action_potential(0.01)
    print "APD90: ", MyocyteModel.find_APD(t, V)
