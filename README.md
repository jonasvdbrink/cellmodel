ODEProblem / CellModel / MyocyteModel
=====================================

This repository includes source code written for solving electrophysiological cell models for human atrial myocytes. The code was developed as part of the thesiswork for an M.Sc., by Jonas van den Brink.

* '/src/' code for ODEProblem, CellModel and MyocyteModel classes
* '/ode/' contains .ode files specifying cell models
* '/data/' contains initial condition data sets for the given .ode files

# Dependencies
* Built on [gotran](https://bitbucket.org/johanhake/gotran/src) by Johan Hake