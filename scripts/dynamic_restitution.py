# -*- encoding: utf-8 -*-

import sys
sys.path.append('../src/')
from MyocyteModel import *
import matplotlib as mpl


def dynamic_restitution(ode):
    """
    Compute APD as a function of PCL following
    a dynamic restitution protocol:

    Starting at PCL = 1000 ms and gradually
    lowering PCL in steps of 10 ms:
        - Myocyte is paced for 30s at every PCL
        - APD90 of 5 consecutive beats is measured
        - PCL is lowered by 10 ms
    """
    dt = 0.01 
    pacetime = 30.*1000 # 30 seconds
    Nbeats = 5
    solver = MyocyteModel(ode, odepath="../ode/")
    PCLs = np.arange(1000, 190, -10)    
    results = np.zeros((len(PCLs), Nbeats+1))
    results[:,0] = PCLs[:]

    for i, PCL in enumerate(PCLs):
        print PCL
        solver.reset_time()
        solver.set_PCL(PCL)
        T = np.ceil((pacetime)/PCL)*PCL
        solver.solve(dt=dt, T=T)
        solver.reset_time()
        for j in range(Nbeats):
            results[i, j+1] = solver.find_APD(*solver.action_potential(dt=dt))
        np.save('../data/results/APD_restituion_dynamic30s_%s.npy' % (ode), results)

if __name__ == '__main__':
    ode = "K2015_nSR"
    dynamic_restitution(ode)


