# -*- encoding: utf-8 -*-

import sys
sys.path.append('../src/')
from MyocyteModel import *
import matplotlib as mpl


def dynamic_APD(ode, gCaL):
    solver = MyocyteModel(ode, odepath="../src/ode/")
    PCLs = np.arange(PCL_start, PCL_stop+PCL_step, PCL_step)
    APDs = np.zeros((len(PCLs), Nbeats+1))
    APDs[:,0] = PCLs[:]
    solver.set_param("gCaL", gCaL)   
    for i, PCL in enumerate(PCLs):
        solver.set_PCL(PCL)
        T = np.ceil((pacetime*1000.)/PCL)*PCL
        solver.steadycycle(dt=dt, T=T, filename="gCaL%.5f_dynamic%ds_PCL%d" % (gCaL, pacetime, PCL), load=False)
        for j in range(Nbeats):
            APDs[i, j+1] = solver.find_APD(*solver.action_potential(dt=dt)) 
    np.save('../data/results/APD_restituion_dynamic30s_%s_gCaL%f.npy' % (ode, gCaL), APDs)

def spatiotemporal_calcium(t, d, **plot_args):
    """Plot spatiotemporal CaT as a heatmap, dataset must contain all Cai"""
    for k in 'Cai1', 'Cai2', 'Cai3', 'Cai4':
        assert k in d, "Dataset must contain all intracellular Ca states"

    CaT = np.zeros((len(t), 5))
    CaT[:, 0] = d['Cai4']
    CaT[:, 1] = (d['Cai4'] + d['Cai3'])/2
    CaT[:, 2] = (d['Cai3'] + d['Cai2'])/2
    CaT[:, 3] = (d['Cai2'] + d['Cai1'])/2
    CaT[:, 4] = d['Cai1']
    CaT = 1e3*CaT
    ip=None
    cmap = None
    # cmap = "gist_heat"
    plt.imshow(CaT.T, aspect='auto', origin='lower', extent=(t[0], t[-1], 0, 4*1.625), **plot_args)


if __name__ == '__main__':
    ode = "K2015_nSR"
    solver = MyocyteModel(ode, odepath="../ode/")
    states = ["Cai1", "Cai2", "Cai3", "Cai4"]

    t, d = solver.tracksolve(dt=0.01, T=600, states=states)

    spatiotemporal_calcium(t, d, cmap=None)
    cbar = plt.colorbar()
    cbar.ax.set_title(ur'   $[\rm Ca^{2+}]_i\ [μM]$', fontsize=20)
    plt.xlabel('Time [ms]')
    plt.ylabel(ur'$r\ [{\rm μm}]$')
    plt.show()
