# -*- encoding: utf-8 -*-

import sys
sys.path.append('../src/')
from MyocyteModel import *
import matplotlib as mpl

K2014 = MyocyteModel("K2014_nSR", odepath="../ode/")
K2015 = MyocyteModel("K2015_nSR", odepath="../ode/")

t, d1 = K2014.tracksolve(dt=0.01, T=600, states=['V'])
t, d2 = K2015.tracksolve(dt=0.01, T=600, states=['V'])

plt.plot(t, d1['V'])
plt.plot(t, d2['V'])
plt.show()